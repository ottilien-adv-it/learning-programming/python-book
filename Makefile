PAPER=grundlagen-programmieren

.PHONY: $(PAPER).pdf all clean

all: $(PAPER).pdf

$(PAPER).pdf: $(PAPER).tex
	latexmk -pdf -pdflatex="pdflatex" -bibtex -synctex=1 -use-make $<

clean:
	latexmk -C
	rm -f *.nav
	rm -f *.snm
	rm -f fig/*-converted-to.pdf

